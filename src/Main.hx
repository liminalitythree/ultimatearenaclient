import h2d.Scene.ScaleMode;

class Main extends hxd.App {
    override function init() {
        this.s2d.scaleMode = ScaleMode.Fixed(300, 300, 1);
        
        var tf = new h2d.Text(hxd.res.DefaultFont.get(), s2d);
        tf.text = "Hello World !";
        
    }
    static function main() {
        new Main();
    }
}
